package com.bibao.boot.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Repository;

import com.bibao.boot.model.Person;

@Repository
public class PersonDaoImpl implements PersonDao {
	private static Map<Integer, Person> personMap;
	
	@PostConstruct
	private void init() {
		personMap = new HashMap<>();
		String[] firstNames = {"Alice", "Bob", "Steven"};
		String[] lastNames = {"Liu", "Zhu", "Zhang"};
		for (int i=0; i<3; i++) {
			Person person = new Person();
			person.setId(i+1);
			person.setFirstName(firstNames[i]);
			person.setLastName(lastNames[i]);
			personMap.put(person.getId(), person);
		}
	}
	
	@Override
	public Person findById(int id) {
		return personMap.get(id);
	}

	@Override
	public List<Person> findAll() {
		return personMap.values().stream().map(p -> p).collect(Collectors.toList());
	}

	@Override
	public Person save(Person person) {
		person.setId(getMaxId() + 1);
		personMap.put(person.getId(), person);
		return person;
	}

	@Override
	public Person update(Person person) {
		if (personMap.containsKey(person.getId())) {
			personMap.put(person.getId(), person);
		}
		return person;
	}

	@Override
	public void deleteById(int id) {
		if (personMap.containsKey(id)) {
			personMap.remove(id);
		}

	}

	private int getMaxId() {
		return personMap.keySet().stream().mapToInt(v -> v).max().orElse(0);
	}
}
